#!/bin/bash

if [ -d /LP ]; then
    echo "This file is only meant to be run once. To configure everything again, exit and re-enter the Docker Container"
    exit 1
fi

# Configure LinuxPermissions
git clone https://gitlab.com/bytepen/education/linux-permissions.git --no-checkout /opt/LP
git -C /opt/LP checkout master -- LinuxPermissions
chmod +x /opt/LP/LinuxPermissions -R

# Setup /LP/random
/opt/LP/LinuxPermissions/LinuxPermissions.py --build
