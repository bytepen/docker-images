#!/usr/bin/env bash
# start-server.sh

chown -R www-data:www-data /opt/app
chown -R www-data:www-data /config

if [ ! -f /config/env ]; then
    echo 'export DJANGO_SECRET_KEY='$(dd if=/dev/urandom bs=60 count=1 | base64 -w 0) > /config/env
    echo 'export DJANGO_PRODUCTION="true"' >> /config/env
    echo 'export REQUESTS_CA_BUNDLE=/etc/ssl/certs/' >> /config/env
    echo '. /config/env' >> ~/.bashrc
fi

source /config/env

if [ ! -f /config/keys/server.key ]; then
    mkdir -p /config/keys/
	SSL_PASS=$(dd if=/dev/urandom bs=256 count=1 | base64 -w 0)
	openssl genrsa -des3 -out /tmp/server.key -passout pass:${SSL_PASS} 2048
    openssl req -new -key /tmp/server.key -passin pass:${SSL_PASS} -out /tmp/server.csr -subj "/C=US/ST=State/L=City/O=Django/CN=localhost"
	openssl rsa -in /tmp/server.key -passin pass:${SSL_PASS} -out /config/keys/server.key
    openssl x509 -req -days 2650 -in /tmp/server.csr -signkey /config/keys/server.key -out /config/keys/server.crt
    rm /tmp/server.key /tmp/server.csr
fi

(cd src; python manage.py migrate)
(cd src; python manage.py collectstatic --no-input)
if [ -n "$DJANGO_SUPERUSER_USERNAME" ] && [ -n "$DJANGO_SUPERUSER_PASSWORD" ] ; then
        (cd src; python manage.py createsuperuser --no-input)
fi
project_name=$(cd src; find . -type f -name wsgi.py | cut -d"/" -f2)
chown www-data:www-data /config/db.sqlite3
(cd src; gunicorn ${project_name}.wsgi --user www-data --bind 0.0.0.0:8010 --workers 3) &
nginx -g "daemon off;"
