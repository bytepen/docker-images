#!/usr/bin/env python3

import bs4
import glob
import json
import requests
import time

class Bot:
    def __init__(self):
        pass

    def read_files(self):
        for filepath in glob.iglob(r'/product-files/*.json'):
            with open(filepath) as f:
                self.check_product(json.loads(f.read()))

    def check_product(self, j):
        resp = requests.get(j["url"], headers=j["headers"])
        soup = bs4.BeautifulSoup(resp.text, "html.parser")
        ret = {
                "url": resp.url
        }
        for key in j['grab']:
            if key != "headers":
                ret[key] = soup.select_one(j['grab'][key]).text
        print(ret)
        self.tell_huginn(j['webhook'], ret)

    def tell_huginn(self, webhook, j):
        resp = requests.post(webhook, data=j)

if __name__ == '__main__':
    while True:
        b = Bot()
        b.read_files()
        time.sleep(60)
