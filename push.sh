#!/bin/bash

main () {
	if [[ -d "./images/${1}" && ! -z "${1}" ]]; then
		image="${1}"
		create_tags "./images/${1}"
	else
		find_images
	fi
}

find_images () {
	for image in ./images/* ; do
		create_tags "${image}"
	done
}

create_tags () {
	for tag in ${1}/* ; do
		build_and_push "${tag}"
	done
}

build_and_push () {
	docker build ${tag} -t "bytepen/$(basename ${image}):$(basename ${tag})"
	docker push "bytepen/$(basename ${image}):$(basename ${tag})"
}

main ${1}
